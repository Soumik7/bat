
Virat Kohli
Virat..Kohli.jpg
Kohli in 2017
Personal information
Born	5 November 1988 (age 29)
Delhi, India
Height	5 ft 9 in (1.75 m)
Batting	Right-handed
Bowling	Right-arm medium
Role	Top-order batsman , Captain
Relations	Anushka Sharma (m. 2017)
Website	www.viratkohli.club
International information
National side	
India (2008-present)
Test debut (cap 269)	20 June 2011 v West Indies
Last Test	18 August 2018 v England
ODI debut (cap 175)	18 August 2008 v Sri Lanka
Last ODI	17 July 2018 v England
ODI shirt no.	18
T20I debut (cap 31)	12 June 2010 v Zimbabwe
Last T20I	8 July 2018 v England
T20I shirt no.	18
Domestic team information
Years	Team
2006�present	Delhi
2008�present	Royal Challengers Bangalore (squad no. 18 / formerly 5)
Career statistics
Competition	Test	ODI	T20I	FC
Matches	67	211	62	98
Runs scored	5,754	9,779	2,102	7,803
Batting average	54.28	58.21	48.88	53.81
100s/50s	22/17	35/48	0/18	27/23
Top score	243	183	90*	243
Balls bowled	163	641	146	631
Wickets	0	4	4	3
Bowling average	�	166.25	49.50	110.00
5 wickets in innings	0	0	0	0
10 wickets in match	0	n/a	n/a	0
Best bowling	�	1/15	1/13	2/42
Catches/stumpings	63/�	101/�	28/�	94/�
Source: ESPNcricinfo, 18 August 2018
Virat Kohli (About this sound pronunciation (help�info); born 5 November 1988) is an Indian international cricketer who currently captains the India national team. An elegant right-handed batsman, Kohli is regarded as one of the best batsmen in the world.[1] He plays for Royal Challengers Bangalore in the Indian Premier League (IPL), and has been the team's captain since 2013.